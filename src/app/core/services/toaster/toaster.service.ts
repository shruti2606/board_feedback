import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToasterService {
  constructor(private toastr: ToastrService) {
  }

  //type = success, error, warning, info, show
  show( message: string,type?: string, title?: string, toastLife?: number) {
    this.toastr[type ? type : 'success'](message, title ? title : '',{
      toastLife: toastLife ? toastLife : 3000,
      newestOnTop: true,
      closeButton: true
    })
  }
}
