import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import {AbstractControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor(private httpClient:HttpClient) { }
  getRequest(url){
    return this.httpClient.get(environment.apiUrl+url);
  }
  postRequest(url,body){
   return this.httpClient.post(environment.apiUrl+url,body);
  }
  emailDomainValidator=function(control: AbstractControl) {
    let pattern = '[a-zA-z_.+0-9-]+@[a-zA-Z0-9-]+([.][a-zA-Z0-9]+)+'
    let email = control.value;
      if(email && !email.match(pattern)){
       return { validEmail: true };
      }   
    return null;
  }
}
