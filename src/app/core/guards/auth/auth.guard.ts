import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { GlobalService } from '../../services/global/global.service';
import { ToasterService } from '../../services/toaster/toaster.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(public router: Router,
    public globalService: GlobalService,
    public toaster: ToasterService) { }

  canActivate(): boolean | Observable<boolean> {
    if (!localStorage.getItem('authtoken')) {
      this.router.navigate(['/login']);
      return false;
    }
    else {
      return this.globalService.postRequest('validatetoken', {})
        .pipe(
          map(
            (response: any) => {

              return true;
            }),
          catchError(
            (err: any) => {
              this.toaster.show(err.message, 'error');
              this.router.navigate(['/login']);
              return of(false);
            })
        );

    }
  }
}
