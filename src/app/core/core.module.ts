import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';
import { GlobalService } from './services/global/global.service';
import { ToasterService } from './services/toaster/toaster.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { ModifyResponseInterceptor } from './interceptors/modify-response.interceptor';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { MenuComponent } from './menu/menu.component';

@NgModule({
  imports: [
    CommonModule,
    ToastrModule.forRoot(),
    RouterModule,
    SharedModule
  ],
  declarations: [MenuComponent],
  providers: [
    GlobalService, ToasterService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ModifyResponseInterceptor, multi: true }
  ]
})
export class CoreModule { }
