import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs";

export class AuthInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let requestOptions: any = {};
        
        // if (localStorage.getItem('authtoken')) {
        //     let logindata: any = {};
        //     let newBody = { ...req.body, logindata: logindata }

        //     let userInfo = JSON.parse(localStorage.getItem('userInfo'));
        //     logindata.loginid = userInfo.user_id;
        //     logindata.type = userInfo.user_type;
            
        //     requestOptions.headers = req.headers.append('csrf_token', localStorage.getItem('authtoken'));
        //     requestOptions.body = newBody;
        // }
        let copiedReq = req.clone(requestOptions)
        return next.handle(copiedReq);
    }
}