import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap } from 'rxjs/operators';
import { ToasterService } from "../services/toaster/toaster.service";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

@Injectable()
export class ModifyResponseInterceptor implements HttpInterceptor {

    constructor(
        private toaster: ToasterService,
        private router: Router) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            tap(res => {
                if (res instanceof HttpResponse) {
                    if (res.status == 200 && res.body['error_code'] == 1) {
                        this.toaster.show(res.body['message'], 'error');
                        setTimeout(() => {
                            this.router.navigate(['/login']);
                        }, 0);
                    }
                }
            }, err => {
                if (err.status == 0) {
                    this.toaster.show('No internet connection!', 'error')
                } else {
                    if (err.status == 500 || err.status == 502) {
                        this.toaster.show('Server error. Please try again after some time.', 'error')
                    } else {
                        this.toaster.show(err.error, 'error')
                        if (err.status == 412)
                            this.router.navigate(['/login']);
                    }
                }
            }
            ))
    }
}