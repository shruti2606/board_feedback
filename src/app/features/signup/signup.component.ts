import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { GlobalService } from '../../core/services/global/global.service';
import { ToasterService } from 'src/app/core/services/toaster/toaster.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  registrationForm: FormGroup;
  isFormInvalid: boolean = false;

  constructor(public fb: FormBuilder,
              private globalService:GlobalService,
              private router:Router,
              private toastr:ToasterService
  ) { }

  ngOnInit() {
   this.LoginValidations();
  }

  submitRegistrationForm() {
    if ( !this.registrationForm.valid) {
      this.isFormInvalid = true;

    } else {
      
      this.isFormInvalid = false;
      console.log(' this.registrationForm.value', this.registrationForm.value)
      this.globalService.postRequest('register',this.registrationForm.value).subscribe(
        (response: any) => {
          console.log("resppponse",response);
          this.toastr.show(response.message + "check mail to reset passwd", 'success');
          this.router.navigate(['login']);
         
        },
        (error) => {
          console.log('errrrrrrrr',error);
          this.toastr.show(error.message, 'error');
          
        }
      );
    }
  }
 
  LoginValidations(){
    this.registrationForm = this.fb.group({
      companyName: [null,
        [
          Validators.required,
          Validators.maxLength(100),
          // Validators.pattern("[a-zA-z_.+0-9-]+@[a-zA-Z0-9-]+([.][a-zA-Z0-9]+)+")
        ]
      ],
      lineOfBusiness: [null,
        [
          Validators.required,
          Validators.maxLength(40),
          // Validators.minLength(6)
        ]
      ],
      websiteAddress: [null,
        [
          Validators.required,
          Validators.maxLength(40),
          // Validators.minLength(6)
        ]
      ],
      emailAddress: [null,
        [
          Validators.required,
          Validators.maxLength(40),
          this.globalService.emailDomainValidator
          // Validators.minLength(6)
        ]
      ],
      businessJustification: [null,
        [
          Validators.required,
          Validators.maxLength(100),
          // Validators.minLength(6)
        ]
      ],
      contactNumber: [null,
        [
          Validators.required,
          Validators.maxLength(10),
          // Validators.pattern("[0-9]{0-10}"),
          // Validators.minLength(10)
        ]
      ],
      organizationType: [null,
        [
          Validators.required,
          Validators.maxLength(20),
          // Validators.minLength(6)
        ]
      ],
      // processingStatus: [null,
      //   [
      //     Validators.required,
      //     Validators.maxLength(20),
      //     // Validators.minLength(6)
      //   ]
      // ],
      organizationLocation: [null,
        [
          Validators.required,
          Validators.maxLength(50),
          // Validators.minLength(6)
        ]
      ],
      
      // Processing: [null,
      //   [
      //     Validators.required,
      //     Validators.maxLength(20),
      //     // Validators.minLength(6)
      //   ]
      // ],
      // remember: [null]
    });
  }

}
