import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { RfqComponent } from './rfq.component';
import { RfqRoutingModule } from './rfq-routing.module';
import { CompanyinfoComponent } from './companyinfo/companyinfo.component';
import { ContactinfoComponent } from './contactinfo/contactinfo.component';
import { BusinessinfoComponent } from './businessinfo/businessinfo.component';
import { ProcurementComponent } from './procurement/procurement.component';
import { CountryComponent } from './country/country.component';
import {TableModule} from 'primeng/table';

@NgModule({
    declarations: [
        RfqComponent,
        CompanyinfoComponent,
        ContactinfoComponent,
        BusinessinfoComponent,
        ProcurementComponent,
        CountryComponent,
    ],
    imports: [
        SharedModule,
        MatButtonModule,
        RfqRoutingModule,
        TableModule
    ],
    providers: [],
    bootstrap: []
})
export class RfqModule { }