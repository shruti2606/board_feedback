import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryComponent implements OnInit {
  cols:any[];
  countryArr:any[];
  constructor(private router:Router,private route:ActivatedRoute) { }

  ngOnInit() {
    this.cols = [
      { field: 'country', header: 'Country/region' },
      { field: 'longname', header: 'Long Names' },
     
  ];
  this.countryArr=[{ country:'ABW',longname:'Aruba'},
  { country:'AGO',longname:'Republic of Angola'},{ country:'INd',longname:'Aruba'},{ country:'ABW',longname:'Aruba'},{ country:'ABW',longname:'Aruba'},{ country:'ABW',longname:'Aruba'},{ country:'ABW',longname:'Aruba'}];
  }

}
