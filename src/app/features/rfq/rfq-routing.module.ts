import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RfqComponent } from './rfq.component';
import { BusinessinfoComponent } from './businessinfo/businessinfo.component';
import { ContactinfoComponent } from './contactinfo/contactinfo.component';
import { CompanyinfoComponent } from './companyinfo/companyinfo.component';
import { CountryComponent } from './country/country.component';
import { ProcurementComponent } from './procurement/procurement.component';



const routes: Routes = [
   { 
        path: '',
        children: [
        {
            path: '',
            component: RfqComponent,
        },
        {
            path: 'country',
            component: CountryComponent,
        },
        {
            path: 'companyinfo',
            component: CompanyinfoComponent,
        },
        {
            path: 'contactinfo',
            component: ContactinfoComponent,
        },
        {
            path: 'businessinfo',
            component: BusinessinfoComponent,
        },
        {
            path: 'procurement',
            component: ProcurementComponent,
        }
    ]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RfqRoutingModule { }
