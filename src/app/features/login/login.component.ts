import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';
import { GlobalService } from '../../core/services/global/global.service';
import { Router } from '@angular/router';
import {ToasterService} from '../../core/services/toaster/toaster.service';
export interface Post {
  id: string;
  title: string;
  content: string;
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})


export class LoginComponent implements OnInit {
   isFormInvalid:boolean=false;
   showPassword:boolean=false;
   private post:Post[]=[];
   loginForm: FormGroup;
   forgotPswdForm: FormGroup;
   showForgotPswdForm:boolean = false;

   constructor(
    public fb: FormBuilder,private globalService:GlobalService,
    private router:Router,private toastr:ToasterService) { }

  ngOnInit() {
    this.initializeLoginForm();
  }
  initializeLoginForm(){
    this.loginForm = this.fb.group({
      login_email: [null,
        [
          Validators.required,
          Validators.maxLength(100),
          this.globalService.emailDomainValidator
        ]
      ],
      login_password: [null,
        [
          Validators.required,
          Validators.maxLength(20),
          Validators.minLength(6)
        ]
      ],
      remember: [null]
    });
  }
  initializeForgotPswdForm(){
    this.forgotPswdForm = this.fb.group({
      forgotPswd_email: [null,
        [
          Validators.required,
          Validators.maxLength(100),
          this.globalService.emailDomainValidator
        ]
      ],
    });   
  }
  gotoforgotPswd(){
    this.initializeForgotPswdForm();
    this.showForgotPswdForm = !this.showForgotPswdForm ;
  }
  gotoLoginForm(){
    this.initializeLoginForm();
    this.showForgotPswdForm = !this.showForgotPswdForm ;
  }
  submitLoginForm(){
    if(this.loginForm.valid){
        this.globalService.postRequest('login',this.loginForm.value).subscribe((response:any)=>{
          if(response.error){
            this.toastr.show(response.message, 'error');
          }else{
          this.router.navigate(['register']);
          this.toastr.show(response.message, 'success');
          }
        },
      err=>{
      console.log('err',err);
      // this.toastr.show(err.message, 'error');

      });
        
    }else{
      this.isFormInvalid=true;
    }
  }
  toggleShowPassword = () =>{
  this.showPassword = !this.showPassword;
  }
 
}
