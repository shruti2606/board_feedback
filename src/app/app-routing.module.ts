import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { MenuComponent } from "./core/menu/menu.component";
import { ForgotpasswordComponent } from "./features/forgotpassword/forgotpassword.component";
import { ResetpasswordComponent } from "./features/resetpassword/resetpassword.component";

const routes: Routes = [
    {
        path: '',
        component: MenuComponent,
        children: [
          
            {
            path: '',
            pathMatch: 'full',
            redirectTo: 'rfq'

          },
          {
            path: 'register',
            loadChildren: './features/rfq/rfq.module#RfqModule',
           
          },
        ]
           
    },
   
    {
        path: 'login',
        loadChildren: './features/login/login.module#LoginModule'
    },
    {
        path: 'signup',
        loadChildren: './features/signup/signup.module#SignupModule'
    },
    {
        path: 'forgot',
        component:ForgotpasswordComponent
    },
    {
        path: 'reset',
        component:ResetpasswordComponent
    },
]
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }