import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

const ESCAPE_KEYCODE = 27; 
const keyPressed = 0;
@Directive({
  selector: '[appRestrictInput]'
})

export class RestrictInputDirective {

    constructor(private el: ElementRef) { }

    // @Input() defaultColor: string;

    @Input('appRestrictInput') limit: number;
    @Input() formName: FormGroup;
    @Input() controlName: string;

    @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
        if(event.srcElement.id == 'contactNumer'){
        if(event.shiftKey || !(event.keyCode == 8|| (event.keyCode >= 48 && event.keyCode <= 57) ) ){
            return false; 
        }
        else if(this.formName.value[this.controlName] && this.formName.value[this.controlName].length >=this.limit) {
          if(event.keyCode == 8){ 
            return true;   }
            return false;
      }
      else 
          return true;
        }
    }

}
