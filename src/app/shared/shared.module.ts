import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";

import { CapitalizePipe } from "./pipe/capitalize.pipe";
import { ErrorMessageComponent } from "./component/error-message/error-message.component";
// import { DialogBoxComponent } from "./component/dialog-box/dialog-box.component";

import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { NgxPaginationModule } from 'ngx-pagination';
import { RestrictInputDirective } from "./directive/restrict-input.directive";
// import {BrowserModule} from '@angular/platform-browser';
// import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// import {TableModule} from 'primeng/table';

@NgModule({
  declarations: [
    CapitalizePipe,
    ErrorMessageComponent,
    RestrictInputDirective,
    
    // DialogBoxComponent,
  ],
  imports: [
    LoadingBarHttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NgxPaginationModule,

    // BrowserModule,
    // BrowserAnimationsModule,
    // TableModule
  ],
  providers: [
  ],
  exports: [CapitalizePipe, LoadingBarHttpClientModule, ErrorMessageComponent, FormsModule, ReactiveFormsModule, CommonModule, NgxPaginationModule,RestrictInputDirective
    ],
  entryComponents: []
})
export class SharedModule { }
