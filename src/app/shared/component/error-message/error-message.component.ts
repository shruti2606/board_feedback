import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.css']
})
export class ErrorMessageComponent implements OnInit {

  @Input() formName: FormGroup;
  @Input() controlName: string;
  @Input() isFormInvalid: boolean;
  @Input() customControlName?: string;
  @Input() mat?: boolean = false;
  @Input() validateEqual: string;

  constructor() {
  }

  ngOnInit() {

  }

}
