const mongoose =require('mongoose');

const vendorSchema = mongoose.Schema({
      companyName : {type : String, required: true},
      lineOfBusiness: {type : String, required: true},
      websiteAddress:{type : String, required: true},
      company_login_id:{type : String, required: true},
      businessJustification:{type : String, required: true},
      contactNumber:{type : String, required: true},
      organizationType: {type : String, required: true},
      organizationLocation:{type : String, required: true},
      password:{type : String, required: true,select:false},
    
});
module.exports = mongoose.model('vendor', vendorSchema);