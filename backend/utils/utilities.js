var generator = require('generate-password');
// const bcrypt = require('bcryptjs');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config=require('../config')


module.exports={
  generatePassword : () => {
    return new Promise((resolve, reject) => {
    let password = generator.generate({
      length: 10,
      numbers: true
    });
    const saltRounds = 10;
      bcrypt.genSalt(saltRounds, function (err, salt) {
        bcrypt.hash(password, salt, function (err, hash) {
            if (err) {
                // reject(err);
                reject({ error: true, message: 'Something went wrong.' });
            }
            else{
              var res={
                hash:hash,
                password:password
              }
              console.log('res------',res);
              resolve(res);
            }
        });
      });
    })
  },
  matchPassword : (data, hash) => {
    return new Promise((resolve, reject) => {
    
      bcrypt.compare(data.login_password, hash, function (err, res) {
        console.log("response",res)
        if (err) {
            reject({ error: true, message: 'Something went wrong.' });
        }
        var token = jwt.sign({ id: data.login_email }, config.jwtSecret,{
            expiresIn : config.expiresIn
        } );
        resolve({ auth: true, token: token });
    });
    })
  }
}

