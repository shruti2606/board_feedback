const mongoose = require('mongoose');
const dbConfig = require('../config').dbConfig;
mongoose.Promise = Promise;
const option={
  socketTimeoutMS:30000,
  keepAlive:true,
  reconnectTries:30000,
  useNewUrlParser: true
};
mongoose.connect(`mongodb://${dbConfig.host}/${dbConfig.database}`,option);
let db = mongoose.connection;
db.on('error', ()=> {
    console.log("Mongo Connection failed.");
});
db.once('open', ()=> {
  console.log("Mongo Connection Successful.");
});