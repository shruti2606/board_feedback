const nodemailer = require('nodemailer');
const mailCreds = require('../config').mailConfig;
module.exports = {
    sendMail:sendMail
};
var transporter = nodemailer.createTransport({
    service: mailCreds.service,
    auth: {
        user: mailCreds.user,
        pass: mailCreds.password
    }
});


// var mailOptions = {
//     to: reqData.emailAddress,
//     cc: 'sapnasinghp2102@gmail.com',
//     subject: 'Registration Confirmation Email !!!!!!',
//     html: `Welcome to Business Express !<br> <b> Registration Confirmation Email !!</b> <br> userid: ${reqData.emailAddress} <br> password : ${password} <br> you can login using this link:  ${linkUrl}`
// };
// if(options.bcc){
//     mailOptions.bcc = options.bcc;
// }
// if(options.to){
//     mailOptions.to = options.email;
// }
// if(options.cc){
//     mailOptions.cc = options.cc;
//     mailOptions.to=options.email;
//     mailOptions.bcc=options.bcc;
// }
// else{
//     mailOptions.to = options.email
// }
function sendMail(mailOptions){
    return new Promise((resolve,reject)=>{
        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                // return reject({mailFailed:true,error:error});
                return reject({ message: 'User Registered Successfully but mail not sent.' });
            }
            console.log('User Registered Successfully.');
            
            resolve({ message: 'User Registered Successfully.' });
        });
    });
}