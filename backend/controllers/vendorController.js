const vendorSchema = require('../schemas/vendorSchema');
const mailer = require('../utils/mailer');
const utilities=require('../utils/utilities');
const validators=require('../utils/utilities');
const vendorModel=require('../models/vendorModel');
module.exports = {  
    login,
    register,
}

function register(req, res, next){
    let reqData = req.body;
    console.log('reqData',reqData);
    if ( !reqData.companyName || !reqData.emailAddress) {
        return res.status(400).send({ error: true, message: 'Required parameters missing.' });
    }
    let password;
  
     vendorModel.findUserByEmail(req.body.emailAddress)
    .then(()=>{
        return utilities.generatePassword()
    })
    .then((result)=>{
        var hash=result.hash;
        password=result.password;
        console.log('password',password);
        const vendorRegistrationData = new vendorSchema({
            companyName: req.body.companyName,
            lineOfBusiness: req.body.lineOfBusiness,
            websiteAddress: req.body.websiteAddress,
            company_login_id: req.body.emailAddress,
            businessJustification: req.body.businessJustification,
            contactNumber: req.body.contactNumber,
            organizationType: req.body.organizationType,
            organizationLocation: req.body.organizationLocation,
            password: hash
        });  
        return vendorModel.signup(vendorRegistrationData)
    })
    .then(()=>{
        let linkUrl = `${jwtConfig.appUrl}`;
        console.log('password22222',password);
        let mailData = {
            to: reqData.emailAddress,
            cc: 'sapnasinghp2102@gmail.com',
            subject: 'Registration Confirmation Email !!!!!!',
            html: `Welcome to Business Express !<br> <b> Registration Confirmation Email !!</b> <br> userid: ${reqData.emailAddress} <br> password : ${password} <br> you can login using this link:  ${linkUrl}`
        };
        return mailer.sendMail(mailData)
    })
    .then((result) => {
        console.log('password');
        // res.send({ message: 'User Registered Successfully.' });
        res.send(result);
    })
    .catch((err)=>{
        res.send(err);
    });
}

function login(req, res, next) {
    let reqData = req.body;
    console.log('reqDData',reqData);
    
    if (!reqData.login_email || !reqData.login_password) {
        return res.status(400).send({ error: true, message: 'Required parameters missng.' });
    }
    vendorModel.checkUserAuthentication(reqData.login_email )
    .then((password)=>{
        
        return utilities.matchPassword(reqData, password)
    })
    .then((result) => {
        result.message= 'logged in Successfully.';
        console.log('result aftr login',result);
        // res.send({ message: 'User Registered Successfully.' });
        res.send(result);
    })
    .catch((err)=>{
        console.log("dshgfskjdhgjdkshgjkds",err);
        
        res.send(err);
    });
    // .then((vendor) => {
    //     console.log("vendorrr",vendor);
        
    //     bcrypt.compare(reqData.login_password, vendor.password, function (err, res) {
    //         if (err) {
    //             return res.status(401).send({ error: true, message: 'Incorrect Email/Password.' });
    //         }
    //         var token = jwt.sign({ id: user_id }, config.secret,{
    //             expiresIn : jwtConfig.expiresIn
    //         } );
    //         res.send({ auth: true, token: token });
    //     });
    // })
    // .catch((err) => {
    //     res.status(422).send({ error: true, message: 'Error Fetching User' });
    // });

  
//           Register.findOne({'company_login_id':req.body.login_email})
//       .then(function(doc) {
//         console.log("docccccccccc",doc);
//              if(!doc)
//                  throw new Error('No record found.');

//           // decrypt pswd
//           var hashPswd = doc.password;
//           var pswd =  req.body.login_password;
//             bcrypt.compare(pswd, hashPswd, function(err, res) {
//               // res == true
//               if(err)
//                     console.log("errror on hashing");
//               else
//                     console.log("match pswd",res);
//             });       
//                   // console.log( 'found record of email ',doc);//else case
//               });

//         res.status(200).send({status:1, message:'yess loged in' });
//     res.status(201).json({
//       message: 'Post added successfully'
//     });

}
  
    // let password = generator.generate({
    //     length: 10,
    //     numbers: true
    // });
    // const saltRounds = 10;
    // const myPlaintextPassword = password;
    // // const someOtherPlaintextPassword = 'not_bacon';

    // bcrypt.genSalt(saltRounds, function (err, salt) {
    //     bcrypt.hash(myPlaintextPassword, salt, function (err, hash) {
    //         // Store hash in your password DB.
    //         if (err)
    //             console.log("errror on hashing");
    //         else {
    //             console.log('hit registerpassword', password)
    //             const vendorRegistrationData = new Register({
    //                 companyName: req.body.companyName,
    //                 lineOfBusiness: req.body.lineOfBusiness,
    //                 websiteAddress: req.body.websiteAddress,
    //                 company_login_id: req.body.emailAddress,
    //                 businessJustification: req.body.businessJustification,
    //                 organizationNumer: req.body.organizationNumer,
    //                 organizationType: req.body.organizationType,
    //                 organizationLocation: req.body.organizationLocation,
    //                 password: hash
    //             });

    //             // vendorRegistrationData.findOne({ email: req.body.email }, function (err, user) {
    //             //   if (err) return res.status(500).send('Error on the server.');
    //             //   if (!user) return res.status(404).send('No user found.');
    //             vendorRegistrationData.save(
    //                 function (err, res) {
    //                     console.log("errrrr,ressss", err, res);

    //                     if (err) {
    //                         console.log("errrrorrr", err);
    //                         res.json({ "state": -1, "message": "User Id or Password is wrong" });
    //                     }
    //                     else {
    //                         console.log("no errr data saved resss", res);
    //                         var transporter = nodemailer.createTransport({
    //                             service: 'gmail',
    //                             auth: {
    //                                 user: 'demotest9375@gmail.com',
    //                                 pass: 'anilrai#90'
    //                             }
    //                         });
    //                         var linkUrl = 'http://localhost:4200/login';
    //                         //   var linkUrl = 'http://192.168.1.76:4200'+'/login?token='+result[0][0].token+'&company_login_id='+data.company_login_id;
    //                         //   var filepath=path.join(__dirname+"../../../views/emailer/emailTemplate.html");
    //                         //   console.log("filepath----=======",filepath);

    //                         //  var readFile =  fs.readFileSync(filepath);

    //                         //  readFile = readFile.toString().replace("linktheurl",linkUrl);
    //                         //  console.log("redfffiilklee",readFile);
    //                         var mailOptions = {
    //                             from: 'demotest9375@gmail.com',
    //                             to: res.company_login_id,
    //                             cc: 'sapnasinghp2102@gmail.com',
    //                             subject: 'Registration Confirmation Email !!!!!!',
    //                             // html: readFile
    //                             text: 'That was easy!',
    //                             html: " Welcome to Business Express !<br> <b> Registration Confirmation Email !!</b> <br> userid: " + res.company_login_id + " <br> password : " + myPlaintextPassword + '<br> you can login using this link:  ' + linkUrl

    //                         };
    //                         console.log("sapmnmmm");

    //                         transporter.sendMail(mailOptions, function (error, info) {
    //                             if (error) {
    //                                 console.log(error, 'hjgjgjhgjhkgjg');
    //                             } else {
    //                                 console.log('Email sent: ' + info.response);
    //                             }
    //                         });
    //                     }
    //                     //  res.status(200).json({message:"Congratulations data entered successfully !!!!"});
    //                     // res.json({ message: "saveddddd" });
    //                 }
    //             );


    //         }

    //     });
    // });

    // res.status(200).json({ message: "saveddddd" });

// function register(req, res, next) {
//     let reqData = req.body;
//     if (!reqData.companyName || !reqData.emailAddress) {
//         return res.status(400).send({ error: true, message: 'Required parameters missng.' });
//     }
//     let password = generator.generate({
//         length: 10,
//         numbers: true
//     });
//     const saltRounds = 10;

//     bcrypt.genSalt(saltRounds, function (err, salt) {
//         bcrypt.hash(password, salt, function (err, hash) {
//             if (err) {
//                 return res.status(422).send({ error: true, message: 'Something went wrong.' });
//             }
//             const vendorRegistrationData = new vendorModel({
//                 companyName: reqData.companyName,
//                 lineOfBusiness: reqData.lineOfBusiness,
//                 websiteAddress: reqData.websiteAddress,
//                 company_login_id: reqData.emailAddress,
//                 businessJustification: reqData.businessJustification,
//                 contactNumber: reqData.contactNumber,
//                 organizationType: reqData.organizationType,
//                 organizationLocation: reqData.organizationLocation,
//                 password: hash
//             });
//             vendorRegistrationData.save()
//                 .then((result) => {
//                     let linkUrl = 'http://localhost:4200/login';
//                     let mailData = {
//                         to: reqData.emailAddress,
//                         cc: 'sapnasinghp2102@gmail.com',
//                         subject: 'Registration Confirmation Email !!!!!!',
//                         html: `Welcome to Business Express !<br> <b> Registration Confirmation Email !!</b> <br> userid: ${reqData.emailAddress} <br> password : ${password} <br> you can login using this link:  ${linkUrl}`
//                     };
//                     return mailer.sendMail(mailData);
//                 })
//                 .then((result) => {
//                     res.send({ message: 'User Registered Successfully.' });
//                 })
//                 .catch((err) => {
//                     console.log('errr',err);
                    
//                     if (err && err.mailFailed) {
//                         res.status(422).send({ message: 'User Registered Successfully but mail not sent.' });
//                     }
//                     else {
//                         res.status(422).send({ message: 'User Registered Failed.' });
//                     }
//                 });
//         });
//     });


//     // let password = generator.generate({
//     //     length: 10,
//     //     numbers: true
//     // });
//     // const saltRounds = 10;
//     // const myPlaintextPassword = password;
//     // // const someOtherPlaintextPassword = 'not_bacon';

//     // bcrypt.genSalt(saltRounds, function (err, salt) {
//     //     bcrypt.hash(myPlaintextPassword, salt, function (err, hash) {
//     //         // Store hash in your password DB.
//     //         if (err)
//     //             console.log("errror on hashing");
//     //         else {
//     //             console.log('hit registerpassword', password)
//     //            
//     //             // vendorRegistrationData.findOne({ email: req.body.email }, function (err, user) {
//     //             //   if (err) return res.status(500).send('Error on the server.');
//     //             //   if (!user) return res.status(404).send('No user found.');
//     //             vendorRegistrationData.save(
//     //                 function (err, res) {
//     //                     console.log("errrrr,ressss", err, res);

//     //                     if (err) {
//     //                         console.log("errrrorrr", err);
//     //                         res.json({ "state": -1, "message": "User Id or Password is wrong" });
//     //                     }
//     //                     else {
//     //                         console.log("no errr data saved resss", res);
//     //                         var transporter = nodemailer.createTransport({
//     //                             service: 'gmail',
//     //                             auth: {
//     //                                 user: 'demotest9375@gmail.com',
//     //                                 pass: 'anilrai#90'
//     //                             }
//     //                         });
//     //                         var linkUrl = 'http://localhost:4200/login';
//     //                         //   var linkUrl = 'http://192.168.1.76:4200'+'/login?token='+result[0][0].token+'&company_login_id='+data.company_login_id;
//     //                         //   var filepath=path.join(__dirname+"../../../views/emailer/emailTemplate.html");
//     //                         //   console.log("filepath----=======",filepath);

//     //                         //  var readFile =  fs.readFileSync(filepath);

//     //                         //  readFile = readFile.toString().replace("linktheurl",linkUrl);
//     //                         //  console.log("redfffiilklee",readFile);
//     //                         var mailOptions = {
//     //                             from: 'demotest9375@gmail.com',
//     //                             to: res.company_login_id,
//     //                             cc: 'sapnasinghp2102@gmail.com',
//     //                             subject: 'Registration Confirmation Email !!!!!!',
//     //                             // html: readFile
//     //                             text: 'That was easy!',
//     //                             html: " Welcome to Business Express !<br> <b> Registration Confirmation Email !!</b> <br> userid: " + res.company_login_id + " <br> password : " + myPlaintextPassword + '<br> you can login using this link:  ' + linkUrl

//     //                         };
//     //                         console.log("sapmnmmm");

//     //                         transporter.sendMail(mailOptions, function (error, info) {
//     //                             if (error) {
//     //                                 console.log(error, 'hjgjgjhgjhkgjg');
//     //                             } else {
//     //                                 console.log('Email sent: ' + info.response);
//     //                             }
//     //                         });
//     //                     }
//     //                     //  res.status(200).json({message:"Congratulations data entered successfully !!!!"});
//     //                     // res.json({ message: "saveddddd" });
//     //                 }
//     //             );


//     //         }

//     //     });
//     // });

//     // res.status(200).json({ message: "saveddddd" });
// }