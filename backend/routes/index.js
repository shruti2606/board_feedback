const express = require('express');
const router = express.Router();
const vendorController = require('../controllers/vendorController');
const authPolicy = require('../policies/authentication');
const utilities = require('../utils/validators');


router.post('/register', vendorController.register);
router.post('/login', vendorController.login);
// router.post(
//     'register', 
//     utilities.validate('register'), 
//     vendorController.register,
//   )

module.exports = router;
