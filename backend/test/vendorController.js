let chai = require('chai');
let server=require('../app')
let expect=chai.expect;
let should = chai.should();
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
describe('/POST register', function() {
    this.timeout(500);
    it('Without parameters', (done) => {
    let vendorRegistrationData = {
        companyName: 'polestar',
        lineOfBusiness: 'polestar',
        websiteAddress: 'polestar',
        emailAddress: 'roma.lakhmani@polestarllp.com',
        businessJustification: 'polestar',
        contactNumber: '9415508988',
        organizationType: 'polestar',
        organizationLocation:'Noida',
    }
    chai.request(server)
    .post('/api/register')
    .send(vendorRegistrationData)
    .end((err, res) => {
      
        expect(vendorRegistrationData).to.exist;
        expect(vendorRegistrationData).to.be.an('object');
        expect(vendorRegistrationData).to.have.all.keys('companyName', 'lineOfBusiness','websiteAddress','emailAddress','businessJustification','contactNumber','organizationType','organizationLocation');
        // expect(vendorRegistrationData).to.have.property('companyName')
        // expect(vendorRegistrationData).to.have.property('lineOfBusiness')
        // expect(vendorRegistrationData).to.have.property('websiteAddress')
        // expect(vendorRegistrationData).to.have.property('emailAddress')
        // expect(vendorRegistrationData).to.have.property('businessJustification')
        // expect(vendorRegistrationData).to.have.property('contactNumber')
        // expect(vendorRegistrationData).to.have.property('organizationType')
        // expect(vendorRegistrationData).to.have.property('organizationLocation')
        expect(res).to.have.status(200);
      done();
    });
})
})
